/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"

class GainSamplerVoice  : public SamplerVoice
{
public:
    GainSamplerVoice(AudioProcessor& myPluginProcessor)
    : parent (myPluginProcessor)
    {
    }
    
    
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* snd, int pitchWheel) override
    {
        if (midiNoteNumber == 36)
            velocity = velocity *  1.5;
        else if (midiNoteNumber == 37)
            velocity = velocity * 1.5;
        
        
        SamplerVoice::startNote(midiNoteNumber, velocity, snd, pitchWheel);
    }
    
    
private:
    AudioProcessor& parent;
    
};




class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
private:
    AudioDeviceManager audioDeviceManager;
    
};

class MidiMessageManager : public MidiInputCallback
{
public:
    
    MidiMessageManager();
    ~MidiMessageManager();
    
    int getCurrentMidiNote();
    float getCurrentMidiNoteHz();
    int getCurrentControlNum();
    int getCurrentControlVal();
    bool isMidiNoteOn();
    bool getControlChange();
    void setControlChangeOff();
    void setMidiNoteOff();
    int getCurrentMidiVelocity();
    
    //required of any superclass of MidiInputCallback.  This is called everytime a midi message is played
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message);
    
private:
    
    MidiInput *midiInput;
    
    int midiNote;
    float midiNoteHz;
    bool midiNoteOn;
    int controlNum;
    int controlVal;
    bool controlChange;
    int velocity;
    
    // (prevent copy constructor and operator= being generated..)
    MidiMessageManager (const MidiMessageManager&);
    const MidiMessageCollector& operator= (const MidiMessageManager&);
};





#endif  // AUDIO_H_INCLUDED
