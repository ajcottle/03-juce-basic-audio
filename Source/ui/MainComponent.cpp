/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    setSize (500, 400);
    VolumeSlider.setSliderStyle (Slider::LinearHorizontal);
    addAndMakeVisible (VolumeSlider);
    VolumeSlider.addListener(this);
    Title.setButtonText ("The Drummer");
    addAndMakeVisible(Title);
    
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    
    VolumeSlider.setBounds(10, 90, getWidth() - 20, 12);
    Title.setBounds(10, 10, getWidth() - 20, 20);
    
}
void MainComponent::paint (Graphics& g)
{
    g.setColour(Colours::coral); 
    g.fillAll();
}
void MainComponent::sliderValueChanged( Slider* slider)
{
    DBG("Slider Moved\n");
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

